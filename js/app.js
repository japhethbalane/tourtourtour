'use strict';
define(['stapes'], function(Stapes){

    var Tour = Stapes.subclass({
        // persist data that the user started tour on date
        // keep track of completed steps
        constructor: function(config) {
            this.index = config.index;
            this.steps = config.steps;
        },
        start: function() {
            this.steps[this.index].execute();
        },
        prev: function() {
            this.steps[this.index].complete();
            if (this.index > 0) {
                this.index--;
            }
            this.steps[this.index].execute();
        },
        next: function() {
            this.steps[this.index].complete();
            this.index++;
            if (this.index >= this.steps.length) {
                this.end();
            }
            this.steps[this.index].execute();
        },
        // persist data that the user once ended the tour on date
        end: function() {
            this.index = 0;
            deactivateBlur();
        },
        destructor: function() {
            delete this;
        }
    });

    var Step = Stapes.subclass({
        // handle configurations
        constructor: function(config) {
            this.element = config.element;
        },
        // called somewhere in the Tour class everytime the user chooses
        // to go to this step
        execute: function() {
            this.element.style.backgroundColor = 'white';
        },
        // complete the step
        complete: function() {
            this.element.style.backgroundColor = 'pink';
        },
    });

    //////////////////////////////////////////////////////////////////////////////

    var els = document.querySelectorAll('.step');
    var steps = [];

    for (var i = 0; i < els.length; i++) {
        var step = new Step({
            element: els[i]
        });
        steps[i] = step;
    }

    var HomeTour = new Tour({
        index: 0,
        steps: steps,
    });

    var start = document.getElementById('start');
    start.addEventListener('click', function(e){
        console.log('Tour Started!');
        activateBlur();
        HomeTour.start();
    });

    var next = document.getElementById('next');
    next.addEventListener('click', function(e){
        HomeTour.next();
    });

    var prev = document.getElementById('prev');
    prev.addEventListener('click', function(e){
        HomeTour.prev();
    });

    // function startTour() {
    //     console.log('Tour Started!');
    //     activateBlur();
    //     HomeTour.start();
    // }

    function activateBlur() {
        var cont = document.querySelector('#container');
        cont.style.backgroundColor = 'pink';
        var cont = document.querySelectorAll('#container *');
        for (var c of cont) c.style.backgroundColor = 'pink';
        var cont = document.querySelector('.start');
        cont.style.display = 'none';
        var cont = document.querySelector('.next');
        cont.style.display = 'block';
        cont.style.backgroundColor = 'white';
        var cont = document.querySelector('.prev');
        cont.style.display = 'block';
        cont.style.backgroundColor = 'white';
    }

    function deactivateBlur() {
        var cont = document.querySelector('#container');
        cont.style.backgroundColor = 'white';
        var cont = document.querySelectorAll('#container *');
        for (var c of cont) c.style.backgroundColor = 'white';
        var cont = document.querySelector('.start');
        cont.style.display = 'block';
        var cont = document.querySelector('.next');
        cont.style.display = 'none';
        cont.style.backgroundColor = 'white';
        var cont = document.querySelector('.prev');
        cont.style.display = 'none';
        cont.style.backgroundColor = 'white';
    }

    // function Next() {
    //     HomeTour.next();
    // }

    // function Prev() {
    //     HomeTour.prev();
    // }
});